//
//  MyCollectionViewCell.swift
//  StudyDrive-Task
//
//  Created by Noor Walid on 08/08/2022.
//

import UIKit

class MyCollectionViewCell: UICollectionViewCell {
    @IBOutlet private weak var cellTextLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.layer.borderWidth = 0.7
        self.layer.borderColor = UIColor.black.cgColor
        self.layer.backgroundColor = UIColor.lightGray.cgColor
    }
    
    func setupCell(text: String) {
        self.cellTextLabel.text =  text
    }

}
