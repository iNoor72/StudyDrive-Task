//
//  ViewController.swift
//  StudyDrive-Task
//
//  Created by Noor Walid on 08/08/2022.
//

import UIKit

class ViewController: UIViewController {
    
    //MARK: IBOutlets
    @IBOutlet private weak var collectionView: UICollectionView!
    
    //MARK: Variables
    private var array = ["Noor", "StudyDrive"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupCollectionView()
    }
    
    private func setupCollectionView() {
        [collectionView].forEach {
            $0?.delegate = self
            $0?.dataSource = self
        }
        
        collectionView.register(UINib(nibName: Constants.XIBs.MyCollectionViewCellXIB, bundle: nil), forCellWithReuseIdentifier: Constants.CollectionViewCells.MyCollectionViewCellID)
        
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .vertical
        flowLayout.itemSize = CGSize(width: (UIScreen.main.bounds.width/4)-14, height: 146)
        flowLayout.minimumLineSpacing = 4.0
        flowLayout.minimumInteritemSpacing = 4.0
        self.collectionView.semanticContentAttribute = .unspecified
        self.collectionView.collectionViewLayout = flowLayout
        self.collectionView.showsHorizontalScrollIndicator = false
    }
    
    private func cellCreator() {
        let _ = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(addCellToCollectionView), userInfo: nil, repeats: true)
    }
    
    private func cellDestroyer() {
        let _ = Timer.scheduledTimer(timeInterval: 4.0, target: self, selector: #selector(removeCellFromCollectionView), userInfo: nil, repeats: true)
    }
    
    @objc private func removeCellFromCollectionView() {
        if (array.count > 0) {
            DispatchQueue.main.async {
                self.array.removeLast()
                self.collectionView.reloadData()
            }
        }
    }
    
    @objc func addCellToCollectionView() {
        DispatchQueue.main.async {
            self.array.append("Hello, I'm a generated cell")
            self.collectionView.reloadData()
        }
    }
    
    //MARK: IBActions
    @IBAction private func addButtonTapped(_ sender: UIButton) {
        cellCreator()
    }
    
    @IBAction private func removeButtonTapped(_ sender: UIButton) {
        cellDestroyer()
    }
}

extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return array.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.CollectionViewCells.MyCollectionViewCellID, for: indexPath) as? MyCollectionViewCell else { return UICollectionViewCell() }
        
        cell.setupCell(text: self.array[indexPath.row])
        
        return cell
    }
}
