//
//  Constants.swift
//  StudyDrive-Task
//
//  Created by Noor Walid on 08/08/2022.
//

import Foundation

struct Constants {
    
    struct CollectionViewCells {
        static let MyCollectionViewCellID = "MyCollectionViewCell"
    }
    
    struct XIBs {
        static let MyCollectionViewCellXIB = "MyCollectionViewCell"
    }
}
