# StudyDrive-Task

StudyDrive-Task is a project build for StudyDrive company.

## Description
This is my implementation for the iOS task requested by StudyDrive company. A simple iOS app that creates a producer and a consumer based on the user's interaction with buttons, it produces and consumes a UICollectionViewCell when tapped.
## Getting started
To run the project, clone the project and run the project directly.
No other steps are needed.

## Features
- Simple and easy functions to do the job of the producer and consumer.
- Using UICollectionView to add and delete the cells.
- A producer that adds a cell every 3 seconds forever.
- A consumer that removes a cell every 4 seconds forever.
- Supporting iOS 15.5 and newer.
- Supporting all of the iPhone devices in the Portrait mode.

## Pods
There's no pods for this project.

## Project status
The project is completed in what was mentioned in the requirements document.
